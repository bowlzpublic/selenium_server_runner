FROM selenium/standalone-chrome:3.14.0

USER root

WORKDIR /app

RUN apt-get update

RUN apt-get -qq -y install samba

COPY smb.conf /etc/samba

COPY run_sel_smb.sh /opt/bin/

RUN service smbd restart

RUN touch /home/seluser/success

RUN (echo secret; sleep 1; echo secret ) | smbpasswd -s -a seluser

RUN chmod +x /opt/bin/run_sel_smb.sh

CMD ["/opt/bin/run_sel_smb.sh"]

USER seluser
